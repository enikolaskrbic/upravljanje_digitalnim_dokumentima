(function () {
	'use strict';
  angular.module('ebookApp', ['ui.router', 'ui.bootstrap', 'LocalStorageModule', 'base64', 'angularFileUpload', 'angularSpinner', 'ngFileSaver', 'ngSanitize']);
    app.directive('myEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if(event.which === 13) {
                    scope.$apply(function (){
                        scope.$eval(attrs.myEnter);
                    });

                    event.preventDefault();
                }
            });
        };
    });
}());