

INSERT INTO category (name) VALUES ('Travel');
INSERT INTO category (name) VALUES ('Childrens');
INSERT INTO category (name) VALUES ('Religion, Spirituality & New Age');
INSERT INTO category (name) VALUES ('Science');
INSERT INTO category (name) VALUES ('History');
INSERT INTO category (name) VALUES ('Math');
INSERT INTO category (name) VALUES ('Poetry');
INSERT INTO category (name) VALUES ('Encyclopedias');
INSERT INTO category (name) VALUES ('Dictionaries');
INSERT INTO category (name) VALUES ('Computer sciences');
INSERT INTO category (name) VALUES ('Autobiographies');

INSERT INTO language (name) VALUES ('English');
INSERT INTO language (name) VALUES ('Serbian');

INSERT INTO users (first_name, last_name, password, user_type, username, category) VALUES ('admin', 'admin', 'admin', 'administrator', 'admin', 1);
INSERT INTO users (first_name, last_name, password, user_type, username, category) VALUES ('subscriber', 'subscriber', 'subscriber', 'subscriber', 'subscriber', 1);


