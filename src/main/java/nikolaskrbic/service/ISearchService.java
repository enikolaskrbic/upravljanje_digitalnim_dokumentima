package nikolaskrbic.service;

import java.util.List;

import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.BooleanClause.Occur;

import nikolaskrbic.endpoints.dto.SearchDto;
import nikolaskrbic.domain.model.Ebook;
import nikolaskrbic.udd.model.RequiredHighlight;
import nikolaskrbic.udd.searcher.SearchField;



public interface ISearchService {
	
	public List<Ebook> search(SearchDto searchDto);
	public Occur getOccur(String value);
	void addToQuery(BooleanQuery bquery, SearchField f);
	void addToQuery(BooleanQuery bquery, List<RequiredHighlight> requiredHighlights,SearchField f);

}
