package nikolaskrbic.service;

import java.util.List;

public interface IService<T> {

	List<T> getAll();
	
	void deleteById(long id);

	T save(T t);

	T findById(long id);
	

}
