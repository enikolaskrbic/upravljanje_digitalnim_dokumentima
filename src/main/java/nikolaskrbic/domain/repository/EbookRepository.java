package nikolaskrbic.domain.repository;

import java.util.List;

import nikolaskrbic.domain.model.Ebook;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface EbookRepository extends JpaRepository<Ebook, Long> {
	
	
	@Query("SELECT ebook FROM Ebook ebook WHERE ebook.category.id = :categoryIdLooking") 
	public  List<Ebook> getEbooksByCategory(@Param("categoryIdLooking") long categoryIdLooking);
	
	
	@Query("SELECT ebook FROM Ebook ebook WHERE ebook.fileName = :fileNameLooking") 
	public Ebook findEbookByFileName(@Param("fileNameLooking") String fileNameLooking);

}

