package nikolaskrbic.domain.repository;

import java.util.List;

import nikolaskrbic.domain.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	
	 @Query("SELECT user FROM User user WHERE user.username = :usernamelooking") 
	 public User findUserByUsername(@Param("usernamelooking") String usernamelooking);
	 
	 @Query("SELECT user FROM User user WHERE user.password = :passwordLooking") 
	 public User passwordExist(@Param("passwordLooking") String passwordLooking);
	 
	 @Query("SELECT user FROM User user WHERE user.category.id = :categoryId") 
	 public List<User> findUsersByCategoryId(@Param("categoryId") long categoryId);

}