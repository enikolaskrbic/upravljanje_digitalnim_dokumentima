package nikolaskrbic.udd.indexer.handler;

import java.io.File;

import nikolaskrbic.domain.model.Ebook;
import nikolaskrbic.udd.exception.IndexDocumentException;
import org.apache.lucene.document.Document;

public abstract class DocumentHandler {
	public abstract Document getDocument(File file) throws IndexDocumentException;
	
	public abstract Document getDocument(File file, Ebook ebook) throws IndexDocumentException;
}
