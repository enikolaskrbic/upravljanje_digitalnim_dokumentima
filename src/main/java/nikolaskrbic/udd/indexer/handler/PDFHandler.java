package nikolaskrbic.udd.indexer.handler;

import java.io.File;
import java.io.FileInputStream;

import nikolaskrbic.domain.model.Ebook;
import nikolaskrbic.udd.exception.IndexDocumentException;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.document.Field.Store;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.util.PDFTextStripper;
import org.springframework.web.multipart.MultipartFile;

public class PDFHandler extends DocumentHandler {
	
	public Document getDocument(MultipartFile file) throws IndexDocumentException {
		Document doc = new Document();
		StringField id = new StringField("id", ""+System.currentTimeMillis(), Store.YES);
		TextField fileNameField = new TextField("fileName",	file.getName(), Store.YES);
		doc.add(id);
		doc.add(fileNameField);
		String error = "";
		try {
			
			PDFParser parser = new PDFParser(file.getInputStream());
			parser.parse();
			PDDocument pdf = parser.getPDDocument();
			
			PDFTextStripper stripper = new PDFTextStripper("utf-8");
			String text = stripper.getText(pdf);
			if(text!=null && !text.trim().equals("")){
				doc.add(new TextField("text", text, Store.NO));
			}else{
				error += "Document without text\n";
			}
			
			PDDocumentInformation info = pdf.getDocumentInformation();

			
			String author = info.getAuthor();
			if(author!=null && !author.trim().equals("")){
				doc.add(new TextField("author", author, Store.YES));
			}else{
				error += "Document without author\n";
			}
			
			String title = info.getTitle();
			if(title!=null && !title.trim().equals("")){
				doc.add(new TextField("title", title, Store.YES));
			}else{
				error += "Document without title";
			}
			
			String keywords = info.getKeywords();
			if(keywords==null){
				keywords = title + " " + author;
			}
			
			String[] kws = keywords.trim().split(" ");
			for(String kw : kws){
				if(!kw.trim().equals("")){
					doc.add(new TextField("keyword", kw, Store.YES));
				}
			}
			
			pdf.close();
		} catch (Exception e) {
			System.out.println("Greska pri konvertovanju pdf dokumenta");
			error += "Document is incomplete. An exception occured";
		}
		
		if(!error.equals("")){
			throw new IndexDocumentException(error.trim());
		}
		
		
		
		return doc;
	}

	@Override
	public Document getDocument(File file) throws IndexDocumentException {
		Document doc = new Document();
		StringField id = new StringField("id", ""+System.currentTimeMillis(), Store.YES);
		TextField fileNameField = new TextField("fileName",	file.getName(), Store.YES);
		doc.add(id);
		doc.add(fileNameField);
		String error = "";
		try {
			StringField locationField = new StringField("location", file.getCanonicalPath(), Store.YES);
			doc.add(locationField);
			PDFParser parser = new PDFParser(new FileInputStream(file));
			parser.parse();
			
			PDDocument pdf = parser.getPDDocument();
			
			PDFTextStripper stripper = new PDFTextStripper("utf-8");
			String text = stripper.getText(pdf);
			if(text!=null && !text.trim().equals("")){
				doc.add(new TextField("text", text, Store.NO));
			}else{
				error += "Document without text\n";
			}
			
			PDDocumentInformation info = pdf.getDocumentInformation();
			
			String author = info.getAuthor();
			if(author!=null && !author.trim().equals("")){
				doc.add(new TextField("author", author, Store.YES));
			}else{
				error += "Document without author\n";
			}
			
			String title = info.getTitle();
			if(title!=null && !title.trim().equals("")){
				doc.add(new TextField("title", title, Store.YES));
			}else{
				error += "Document without title";
			}
			
			String keywords = info.getKeywords();
			if(keywords==null){
				keywords = title + " " + author;
			}
			
			String[] kws = keywords.trim().split(" ");
			for(String kw : kws){
				if(!kw.trim().equals("")){
					doc.add(new TextField("keyword", kw, Store.YES));
				}
			}
			
			pdf.close();
		} catch (Exception e) {
			System.out.println("Greska pri konvertovanju pdf dokumenta");
			error = "Document is incomplete. An exception occured";
		}
		
		if(!error.equals("")){
			throw new IndexDocumentException(error.trim());
		}
		
		return doc;
	}

	@Override
	public Document getDocument(File file, Ebook ebook) throws IndexDocumentException {
		// TODO Auto-generated method stub
		Document doc = getDocument(file);
		
		System.out.println("pre brisanja polja: " + doc.getValues("keyword").length);
		System.out.println("pre brisanja polja get: " + doc.get("keyword"));
		
		doc.removeField("author");
		doc.removeField("title");
		//doc.removeField("keyword");
		doc.removeFields("keyword");
		
		System.out.println("Posle brisanja polja: " + doc.getValues("keyword").length);
		System.out.println("Posle brisanja polja get: " + doc.get("keyword"));
		
		System.out.println("Get document keywords: " + ebook.getKeywords());
		String[] kws = ebook.getKeywords().trim().split(" ");
		System.out.println("Duzina niza stringa: " + kws.length);
		for(String kw : kws){
			if(!kw.trim().equals("")){
				System.out.println("Dodajem keywordd: " + kw);
				doc.add(new TextField("keyword", kw, Store.YES));
			}
		}
		doc.add(new TextField("author", ebook.getAuthor(), Store.YES));
		doc.add(new TextField("title", ebook.getTitle(), Store.YES));
		
		
		doc.add(new StringField("ebookId", ebook.getId().toString(), Store.YES));
		doc.add(new StringField("publicationYear", ebook.getPublicationYear().toString(), Store.YES));
		doc.add(new StringField("mime", ebook.getMime(), Store.YES));
		System.out.println("File name of the book in document: " + ebook.getFileName());
		doc.add(new StringField("fileName", ebook.getFileName(), Store.YES));
		System.out.println("Cateogry name of the book in document: " + ebook.getCategory().getName());
		doc.add(new StringField("category", ebook.getCategory().getName(), Store.YES));
		doc.add(new TextField("language", ebook.getLanguage().getName(), Store.YES));
		System.out.println("User name of the book in document: " + ebook.getUsers().getUsername());
		doc.add(new TextField("user", ebook.getUsers().getUsername(), Store.YES));

		doc.forEach(System.out::println);

		return doc;
	}

}
