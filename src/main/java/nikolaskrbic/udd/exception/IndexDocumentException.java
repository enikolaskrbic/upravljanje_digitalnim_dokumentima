package nikolaskrbic.udd.exception;

@SuppressWarnings("serial")
public class IndexDocumentException extends Exception {
	
	public IndexDocumentException(){
		super("Incomplete Document. Default metadata is not present");
	}
	
	public IndexDocumentException(String message){
		super(message);
	}

}
