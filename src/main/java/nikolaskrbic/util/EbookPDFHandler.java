package nikolaskrbic.util;

import nikolaskrbic.domain.model.Ebook;
import nikolaskrbic.udd.exception.IndexDocumentException;
import org.apache.lucene.document.Document;
import org.springframework.web.multipart.MultipartFile;

import nikolaskrbic.udd.indexer.handler.PDFHandler;

public class EbookPDFHandler {
	
	public static Ebook createEbookFromPDF(MultipartFile file){
		
		PDFHandler pdfHandler = new PDFHandler();
		Document document = null;
		try {
			document = pdfHandler.getDocument(file);
		} catch (IndexDocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Ebook ebook = new Ebook();
		ebook.setAuthor(document.get("author"));
		ebook.setTitle(document.get("title"));
		
			ebook.setId(System.currentTimeMillis());
		
		String[] allKeywords = document.getValues("keyword");
		String 	temp = "";
		for(String keyword : allKeywords){
			temp += keyword + " ";
		}
		
		ebook.setKeywords(temp);
			
		return ebook;
		
	}
	
	
}
