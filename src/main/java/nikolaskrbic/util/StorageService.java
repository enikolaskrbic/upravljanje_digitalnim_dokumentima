package nikolaskrbic.util;

import org.apache.lucene.document.Document;
import org.springframework.web.multipart.MultipartFile;

import nikolaskrbic.domain.model.Ebook;

public interface StorageService {

	void init();

	void store(MultipartFile file, Ebook ebook);
	
	void deleteFile(Document doc);

   
}
