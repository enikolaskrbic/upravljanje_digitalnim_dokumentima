package nikolaskrbic.endpoints;

import java.util.List;

import org.apache.commons.logging.impl.Log4JLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import nikolaskrbic.endpoints.dto.CategoryDTO;
import nikolaskrbic.domain.model.Category;
import nikolaskrbic.service.CategoryService;

@RestController
@RequestMapping("/api")
public class CategoryEndpoint {
	
	@Autowired
	CategoryService categoryService;
	
	private static Log4JLogger logger = new Log4JLogger(CategoryEndpoint.class.getName());

	
	@RequestMapping(value = "/categories", method = RequestMethod.GET)
	public ResponseEntity<List<Category>> getCategories() {
		return new ResponseEntity<List<Category>>(categoryService.getAll(), HttpStatus.OK);
	}

    	
	@RequestMapping(value = "/editCategory", method = RequestMethod.PUT)
    public ResponseEntity<Category> updateCategory(@RequestBody CategoryDTO categoryDto) {
        return categoryService.updateCategory(categoryDto);
    }

    @RequestMapping(value = "/categoryAdd", method = RequestMethod.POST)
    public ResponseEntity<Category> addCategory(@RequestBody String categoryName) {
        return categoryService.addCategory(categoryName);
    }
    
    @RequestMapping(value = "/deleteCategory", method = RequestMethod.DELETE)
    public ResponseEntity<Category> deleteCategory(@RequestBody long categoryId) {
        return categoryService.deleteCategory(categoryId);
    }

}

