package nikolaskrbic.endpoints.dto;

import java.util.List;

import nikolaskrbic.udd.searcher.SearchField;

public class SearchDto {
	
	private List<SearchField> fields;

	public List<SearchField> getFields() {
		return fields;
	}

	public void setFields(List<SearchField> fields) {
		this.fields = fields;
	}

	@Override
	public String toString() {
		return "SearchDto [fields=" + fields + "]";
	}

	

}
