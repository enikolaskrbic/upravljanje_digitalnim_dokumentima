package nikolaskrbic.endpoints;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import nikolaskrbic.endpoints.dto.EbookAddDTO;
import nikolaskrbic.endpoints.dto.EbookDTO;
import nikolaskrbic.domain.model.Ebook;
import nikolaskrbic.service.EbookService;
import javassist.NotFoundException;

@RestController
@RequestMapping("/api")
public class EbookEndpoint {
	
	@Autowired
	EbookService ebookService;
			
	private static final int BUFFER_SIZE = 4*1024;
	
	@RequestMapping(value = "/ebooks", method = RequestMethod.GET)
	public ResponseEntity<List<Ebook>> getEbooks() {
		return new ResponseEntity<List<Ebook>>(ebookService.getAll(), HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/ebooksByCategory", method = RequestMethod.POST)
	public ResponseEntity<List<Ebook>> getEbooksByCategory(@RequestBody Long categoryId) {		
		return new ResponseEntity<List<Ebook>>(ebookService.getEbooksByCategory(categoryId), HttpStatus.OK);
	}

	
	@RequestMapping(value = "/getEbookData", method = RequestMethod.POST)
    public ResponseEntity<Ebook> editEbook(@RequestBody Long ebookId) {
        return new ResponseEntity<Ebook>(ebookService.findById(ebookId), HttpStatus.OK);
    }
	
	@RequestMapping(value = "/ebookEdit", method = RequestMethod.POST)
	public ResponseEntity<Ebook> editEbook(@RequestPart("ebook") EbookDTO ebookDto, @RequestPart(name="file",required=false) MultipartFile file) {
	    if(file==null){
			Ebook ebookToUpdate = ebookService.findById(ebookDto.getEbookId());
			Ebook ebook = ebookService.updateEbookWithoutFile(ebookToUpdate, ebookDto);
			if (ebook == null) {
		    	//If file name already exists
		    	return new ResponseEntity<>(HttpStatus.CONFLICT);
		    }
			System.out.println(ebook.getAuthor() + " and " + ebook.getTitle());
			return new ResponseEntity<Ebook>(ebook, HttpStatus.CREATED);
		}else{
			ebookService.deleteById(ebookDto.getEbookId());
			Ebook ebook = ebookService.updateEbookWithFile(ebookDto, file);
			return new ResponseEntity<Ebook>(ebook, HttpStatus.CREATED);
		}               
	}

    
    @RequestMapping(value = "/deleteEbook", method = RequestMethod.DELETE)
    public ResponseEntity<Ebook> deleteEbook(@RequestBody Long ebookId) throws NotFoundException {
        ebookService.deleteById(ebookId);
        return new ResponseEntity<Ebook>(HttpStatus.OK);
    }

	@RequestMapping(value = "/ebookAdd", method = RequestMethod.POST)
	public ResponseEntity<Ebook> addEbook(@RequestPart("ebook") EbookAddDTO ebookAddDto, @RequestPart("file") MultipartFile file) {
	    Ebook ebook = ebookService.addEbook(ebookAddDto, file);
	    if (ebook == null) {
	    	return new ResponseEntity<>(HttpStatus.CONFLICT);
	    }   
	   return new ResponseEntity<Ebook>(ebook, HttpStatus.CREATED);
	}

	
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public ResponseEntity<Ebook> upload(@RequestBody MultipartFile file) {    
	    return new ResponseEntity<Ebook>(ebookService.upload(file), HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/downloadEbook/{fileName}", method = RequestMethod.GET)
	public void downloadEbook(HttpServletRequest request, HttpServletResponse response, @PathVariable("fileName") String fileName) throws IOException {
		System.out.println("Download filename: " + fileName);	
		if(!fileName.endsWith(".pdf")){
			fileName+=".pdf";
        }
		
		String filePath = ResourceBundle.getBundle("index").getString("docs") + File.separator + fileName;
		File downloadFile = new File(filePath);
		System.out.println(downloadFile.getName());
        FileInputStream inputStream = new FileInputStream(downloadFile);
        
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/pdf");    
		response.addHeader("Content-Disposition", "attachment; filename=" + downloadFile.getName());
		response.setContentLength((int) downloadFile.length());
		       
        OutputStream outStream = response.getOutputStream();
        
        byte[] buffer = new byte[BUFFER_SIZE];
        int bytesRead = -1;

        while ((bytesRead = inputStream.read(buffer)) != -1) {
            outStream.write(buffer, 0, bytesRead);
        }
 
        inputStream.close();
        outStream.close();	
	}	
}
