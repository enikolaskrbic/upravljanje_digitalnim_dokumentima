package nikolaskrbic.endpoints;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import nikolaskrbic.domain.model.Language;
import nikolaskrbic.service.LanguageService;

@RestController
@RequestMapping("/api")
public class LanguageEndpoint {
	
	@Autowired
	LanguageService languageService;
	
	@RequestMapping(value = "/languages", method = RequestMethod.GET)
	public ResponseEntity<List<Language>> getLanguages() {
		return new ResponseEntity<List<Language>>(languageService.getAll(), HttpStatus.OK);
	}

}