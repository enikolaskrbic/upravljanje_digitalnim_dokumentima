package nikolaskrbic.endpoints;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import nikolaskrbic.endpoints.dto.CredentialsDTO;
import nikolaskrbic.endpoints.dto.UserAdminDTO;
import nikolaskrbic.endpoints.dto.UserDTO;
import nikolaskrbic.domain.model.User;
import nikolaskrbic.service.UserService;

@RestController
@RequestMapping("/api")
public class UserEndpoint {
	
	@Autowired
	UserService userService;

	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public ResponseEntity<List<User>> getUsers() {
		return new ResponseEntity<List<User>>(userService.getAll(), HttpStatus.OK);
	}

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<User> login(@RequestBody CredentialsDTO credentialsDto) {
        return userService.login(credentialsDto);
    }

    
    @RequestMapping(value = "/editUser", method = RequestMethod.PUT)
    public ResponseEntity<User> updateUser(@RequestBody UserDTO userDto) {
        return userService.updateUser(userDto);
    }
    
    @RequestMapping(value = "/editUserByAdmin", method = RequestMethod.PUT)
    public ResponseEntity<User> editUserByAdmin(@RequestBody UserAdminDTO userAdminDto) {
        return userService.editUserByAdmin(userAdminDto);
    }

    @RequestMapping(value = "/userAdd", method = RequestMethod.POST)
    public ResponseEntity<User> createUser(@RequestBody UserAdminDTO userDto) {
        return  userService.createUser(userDto);
    }
    
    @RequestMapping(value = "/deleteUser", method = RequestMethod.DELETE)
    public ResponseEntity<User> deleteUser(@RequestBody long userId) {
        return userService.deleteUser(userId);
    }

    @RequestMapping(value = "/getUserById", method = RequestMethod.POST)
    public ResponseEntity<User> getUserById(@RequestBody long userId) {       
        return userService.getUserById(userId);
    }

}
